import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class mainApp extends JFrame {

	private JPanel contentPane;
	private JTextField firstNumberTextField;
	private JTextField secondNumberTextField;
	private JTextField resultTextField;

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				
				try {
					
					mainApp frame = new mainApp();
					frame.setVisible(true);
					
				} catch (Exception e) {
					
					e.printStackTrace();
					
				}
				
			}
			
		});
		
	}

	public mainApp() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 430, 270);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		JLabel titleLabel = new JLabel("Mini Calculadora");
		titleLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		sl_contentPane.putConstraint(SpringLayout.NORTH, titleLabel, 10, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, titleLabel, 10, SpringLayout.WEST, contentPane);
		contentPane.add(titleLabel);
		
		JLabel numberLabel = new JLabel("Introduce dos n\u00FAmeros para calcular");
		sl_contentPane.putConstraint(SpringLayout.NORTH, numberLabel, 6, SpringLayout.SOUTH, titleLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, numberLabel, 10, SpringLayout.WEST, contentPane);
		contentPane.add(numberLabel);
		
		firstNumberTextField = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, firstNumberTextField, 6, SpringLayout.SOUTH, numberLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, firstNumberTextField, 0, SpringLayout.WEST, titleLabel);
		contentPane.add(firstNumberTextField);
		firstNumberTextField.setColumns(10);
		
		secondNumberTextField = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, secondNumberTextField, 0, SpringLayout.NORTH, firstNumberTextField);
		contentPane.add(secondNumberTextField);
		secondNumberTextField.setColumns(10);
		
		JLabel equalLabel = new JLabel("=");
		sl_contentPane.putConstraint(SpringLayout.NORTH, equalLabel, 3, SpringLayout.NORTH, firstNumberTextField);
		sl_contentPane.putConstraint(SpringLayout.WEST, equalLabel, 6, SpringLayout.EAST, secondNumberTextField);
		contentPane.add(equalLabel);
		
		resultTextField = new JTextField();
		resultTextField.setEditable(false);
		sl_contentPane.putConstraint(SpringLayout.NORTH, resultTextField, 0, SpringLayout.NORTH, firstNumberTextField);
		sl_contentPane.putConstraint(SpringLayout.WEST, resultTextField, 6, SpringLayout.EAST, equalLabel);
		contentPane.add(resultTextField);
		resultTextField.setColumns(10);
		
		JLabel operationLabel = new JLabel("+-*/");
		sl_contentPane.putConstraint(SpringLayout.WEST, secondNumberTextField, 6, SpringLayout.EAST, operationLabel);
		sl_contentPane.putConstraint(SpringLayout.NORTH, operationLabel, 3, SpringLayout.NORTH, firstNumberTextField);
		sl_contentPane.putConstraint(SpringLayout.WEST, operationLabel, 6, SpringLayout.EAST, firstNumberTextField);
		contentPane.add(operationLabel);
		
		JLabel operationTypeLabel = new JLabel("Selecciona el tipo de operaci\u00F3n que quieres realizar");
		sl_contentPane.putConstraint(SpringLayout.NORTH, operationTypeLabel, 6, SpringLayout.SOUTH, firstNumberTextField);
		sl_contentPane.putConstraint(SpringLayout.WEST, operationTypeLabel, 0, SpringLayout.WEST, titleLabel);
		contentPane.add(operationTypeLabel);
		
		JButton addButton = new JButton("Suma");
		sl_contentPane.putConstraint(SpringLayout.NORTH, addButton, 6, SpringLayout.SOUTH, operationTypeLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, addButton, 0, SpringLayout.WEST, titleLabel);
		contentPane.add(addButton);
		
		JButton subButton = new JButton("Resta");
		sl_contentPane.putConstraint(SpringLayout.NORTH, subButton, 6, SpringLayout.SOUTH, operationTypeLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, subButton, 6, SpringLayout.EAST, addButton);
		contentPane.add(subButton);
		
		JButton mulButton = new JButton("Multiplicaci\u00F3n");
		sl_contentPane.putConstraint(SpringLayout.NORTH, mulButton, 0, SpringLayout.NORTH, addButton);
		sl_contentPane.putConstraint(SpringLayout.WEST, mulButton, 6, SpringLayout.EAST, subButton);
		contentPane.add(mulButton);
		
		JButton divButton = new JButton("Divisi\u00F3n");
		sl_contentPane.putConstraint(SpringLayout.NORTH, divButton, 6, SpringLayout.SOUTH, operationTypeLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, divButton, 6, SpringLayout.EAST, mulButton);
		contentPane.add(divButton);
		
		JButton exitButton = new JButton("Salir");
		sl_contentPane.putConstraint(SpringLayout.SOUTH, exitButton, 0, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, exitButton, -10, SpringLayout.EAST, contentPane);
		contentPane.add(exitButton);
		
		JButton aboutButton = new JButton("Acerca de");
		sl_contentPane.putConstraint(SpringLayout.NORTH, aboutButton, 0, SpringLayout.NORTH, exitButton);
		sl_contentPane.putConstraint(SpringLayout.WEST, aboutButton, 0, SpringLayout.WEST, titleLabel);
		aboutButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		contentPane.add(aboutButton);
		
		addButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				int firstParsedNumber = Integer.parseInt(firstNumberTextField.getText());
				int secondParsedNumber = Integer.parseInt(secondNumberTextField.getText());
				
				int resultParsedNumber = firstParsedNumber + secondParsedNumber;
				
				resultTextField.setText(String.valueOf(resultParsedNumber));
				
			}
			
		});
		
		subButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				int firstParsedNumber = Integer.parseInt(firstNumberTextField.getText());
				int secondParsedNumber = Integer.parseInt(secondNumberTextField.getText());
				
				int resultParsedNumber = firstParsedNumber - secondParsedNumber;
				
				resultTextField.setText(String.valueOf(resultParsedNumber));
				
			}
			
		});
		
		mulButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				int firstParsedNumber = Integer.parseInt(firstNumberTextField.getText());
				int secondParsedNumber = Integer.parseInt(secondNumberTextField.getText());
				
				int resultParsedNumber = firstParsedNumber * secondParsedNumber;
				
				resultTextField.setText(String.valueOf(resultParsedNumber));
				
			}
			
		});
		
		divButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				int firstParsedNumber = Integer.parseInt(firstNumberTextField.getText());
				int secondParsedNumber = Integer.parseInt(secondNumberTextField.getText());
				
				int resultParsedNumber = firstParsedNumber / secondParsedNumber;
				
				resultTextField.setText(String.valueOf(resultParsedNumber));
				
			}
			
		});
		
		aboutButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				JOptionPane.showMessageDialog(null, "Copyright (c) 2020 Dapase\r\n" +
				"Todos los derechos reservados\r\n" +
						" \r\n" +
				"Este producto está protegido por copyright y distribuido bajo\r\n" +
						"licencias que restringen la copia, distribución, y decompilación.");
				
			}
			
		});
		
		exitButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				int confirmExit = JOptionPane.showConfirmDialog(null, "Estás seguro de que quieres salir?", "Confirm Quit", JOptionPane.YES_NO_OPTION);
				
				if (confirmExit == JOptionPane.YES_OPTION) {
					
					System.exit(0);
					
				}
				
			}
			
		});
		
	}
	
}
